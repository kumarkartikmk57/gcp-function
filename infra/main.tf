provider "google" {
  version = "4.57.0"
  project = var.projectid
  region  = var.region
  zone    = var.zone
}

#storage_module

module "gcp-cloud-storge"{
    source = "./modules/gcpStorage"
    bucket_name = var.bucket_name
    bucket_location = var.bucket_location
    projectid = var.projectid
    environment = var.environment
    bucket_object_name = var.bucket_object_name
    bucket_object_path = var.bucket_object_path
}

#gcp_function_module
module "gcp-cloud-function" {
  source = "./modules/gcpCloudFunction"
  function_name = var.function_name
  function_runtime = var.function_runtime
  function_memory = var.function_memory
  environment = var.environment
  bucket_name = var.bucket_name
  bucket_object_name = var.bucket_name
  
}
