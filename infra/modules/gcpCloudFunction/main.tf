terraform {
  required_version = ">=1.4.0"
}
resource "google_cloudfunctions_function" "function" {
    name = "${var.environment}-${var.function_name}"
    runtime = var.function_runtime
    available_memory_mb = var.function_memory
    source_archive_bucket = var.bucket_name
    source_archive_object = var.bucket_object_name
    trigger_http = true
}

resource "google_cloudfunctions_function_iam_member" "invoker" {
    cloud_function = google_cloudfunctions_function.function.name
    role = "roles/cloudfunctions.invoker"
    member = "allUsers"
  
}
