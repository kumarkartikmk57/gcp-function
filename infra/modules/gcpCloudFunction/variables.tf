variable "function_name" {
    type = string  
}
variable "environment" {
    type = string
    default = "uat"  
}
variable "function_runtime" {
    type = string
    default = "python39"  
}
variable "function_memory" {
    type = number
    default = "128"  
}
variable "bucket_name" {
    type = string  
}
variable "bucket_object_name" {
    type = string  
}