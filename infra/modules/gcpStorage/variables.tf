variable "projectid" {
    type = string  
}

variable "environment" {
    type = string 
    default = "uat" 
}

variable "bucket_name" {
    type = string  
}

variable "bucket_location" {
    type = string
    default = "EU"  
}

variable "bucket_object_name" {
    type = string  
}

variable "bucket_object_path" {
    type = string
  
}