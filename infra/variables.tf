variable "projectid" {
    type = string  
}

variable "environment" {
    type = string  
    default = "uat"
}

variable "bucket_name" {
    type = string  
}

variable "bucket_location" {
    type = string
    default = "europe-west1"  
}

variable "bucket_object_name" {
    type = string  
}

variable "bucket_object_path" {
    type = string
  
}

variable "region" {
    type = string
    default = "europe-west1"  
}

variable "zone" {
    type = string
    default = "europe-west1-a"  
}
variable "function_name" {
    type = string
  
}
variable "function_memory" {
    type = number
    default = "128"  
}
variable "function_runtime" {
    type = string
    default = "python39"  
}